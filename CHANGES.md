### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

#### [v0.6.9](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.6.8...v0.6.9)

> 19 March 2024

- Add support for RIFT versions up to 0.0.15.11

#### [v0.6.8](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.6.7...v0.6.8)

> 25 February 2024

- convert pastro from 128bit to 64bit to save as json !172

#### [v0.6.7](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.6.6...v0.6.7)

> 13 February 2024

- fixed description for find_sigma !169

#### [v0.6.6](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.6.5...v0.6.6)

> 6 February 2024

- Modified config.seed to make it optional !168
- corrected missing sigma factor in posterior sampler !151

#### [v0.6.5](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.6.4...v0.6.5)

> 29 November 2023

- exiting post scripts if any margll is inf !165

#### [v0.6.4](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.6.3...v0.6.4)

> 9 November 2023

- Template m1 m2 ordering fix !164
- Removed an erroneous Condor quote escape !163
- Adding options for setting environment variables used by Condor !162
- additional function to renormalize pastro with pipeline pterr !159

#### [v0.6.3](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.6.2...v0.6.3)

> 18 August 2023

- updated logic for mapping to injection for MDC !156
- updated initial region bounds for bbh !155

#### [v0.6.2](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.6.1...v0.6.2)

> 17 August 2023

- updated fmin_template for pycbc !154

#### [v0.6.1](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.6.0...v0.6.1)

> 17 August 2023

- remove inf and nan from margll !152
- bug fix in search_bias json !153
- compute pastro for all pipelines !150
- run rapidpe on all pipelines !149

#### [v0.6.0](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.5.3...v0.6.0)

> 8 August 2023

- using pipeline p_terr !148
- Excluding LALSuite 7.15 !145

#### [v0.5.3](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.5.2...v0.5.3)

> 10 July 2023

- updated to rift==0.0.15.9 !144
- Updating several CI job Docker images to Python 3.9 8f3b559628f4627b5b394b83414553c768647b74
- Removing Python 3.8 from CI pipeline 3b220548fb4d38dc165a4aadc45c94551cbe16be

#### [v0.5.2](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.5.1...v0.5.2)

> 4 July 2023

- Returning a special exit code when no GstLAL trigger available !141

#### [v0.5.1](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.5.0...v0.5.1)

> 9 June 2023

- fixed paths in cache files !140

#### [v0.5.0](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.4.0...v0.5.0)

> 8 June 2023

- Adding option to get strain data from shared memory, cleaning up GWDataFind !139
- Adding option to get strain data from Kafka, cleaning up GWDataFind 723e72cf14f79309f593773b827273c744c0390b
- Improvements to naming conventions, default settings, and docs f5903c6323d50b55341e0908034f99f05a91e480
- Using frametype correctly with GWPy 1b79e460eb2ad8b951d3079029b4820f3e3fcf17

#### [v0.4.0](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.3.2...v0.4.0)

> 1 June 2023

- added float128 in exp(margL) !138
- Updated rift version to 0.0.15.8 2f542dd6d8ac2936f38f6ff3fece0a2106de03e9

#### [v0.3.2](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.3.1...v0.3.2)

> 31 May 2023

- Bug fix on data time !137

#### [v0.3.1](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.3.0...v0.3.1)

> 30 May 2023

- updated start and end time for data cache !136
- added retries in gracedb and gwdatafind query !135
- updated start and end time for reading frame data c5453ef615619a76afa34595f33390b20652e583

#### [v0.3.0](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.2.2...v0.3.0)

> 18 May 2023

- Added option to specify channel names based on mode we're running in !134

#### [v0.2.2](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.2.1...v0.2.2)

> 9 May 2023

- fixed import error !133
- updates for pastro calculation !132
- added use_event_spin for injections !130
- using SEOBNRv4 for mchirp>2.6 cc92a7819e87d1d417322466ad684eaf341b43d4
- fixed plot axis limits for grid plots e172675872040543ed053d892fd5f7ad43e419b1

#### [v0.2.1](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.2.0...v0.2.1)

> 22 April 2023

- making mdc related config options optional !129

#### [v0.2.0](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.1.0...v0.2.0)

> 22 April 2023

- checking if injection based on channel name !128
- Pastro rates !127
- modified summary page and dir !126
- saving skymap as fits !125
- removed convergence from summary page. renamed summary_plots to summary. 094679c654751946acf3429e071ddf4a5453d259
- added injection and non-injection rates in pastro calculation 984f02f7690477628e62f62a2effb59a5a6099fa

#### [v0.1.0](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.12...v0.1.0)

> 20 April 2023

- Adding empty Pastro section to all example configs to fix unit test !122
- modified bins in posterior plot !121
- Added pastro !119
- updated default.json !120
- moved cprofile to General section of the config file !118
- pastro 5a10f0e3856dc73cdcc7b5c51386461ee64c999f
- moved pastro to class d6f6ed945111ec493fa5e1f11747152df57e3270

#### [v0.0.12](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.11...v0.0.12)

> 7 April 2023

- Allowing for upcoming or current RIFT release !115
- correctly identifying 4d runs in post scripts and fixing a bug in profile !114
- added injection mapping for both sid and gid !113
- Fixing static file access !112
- for MDC, added injection point for all plots !111

#### [v0.0.11](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.10...v0.0.11)

> 3 April 2023

- Fix typo in 'pin-param' !109

#### [v0.0.10](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.9...v0.0.10)

> 31 March 2023

- fixed bug in boundary check for postscript !108
- updates to submit dag from any dir !107
- rapid_pe version to 0.0.6 d57327989530f3c09354b796a7f05babe5f4c0f1

#### [v0.0.9](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.8...v0.0.9)

> 23 March 2023

- added accounting-group-user in condor sub files !106
- changing rapid_pe to 0.0.5 d2d73f8dc04acc6738a862433e8a37818a8da564

#### [v0.0.8](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.7...v0.0.8)

> 13 March 2023

- fixed bug in finding the iteration level in postscripts !105
- fix prior 4d05d7d6b06982bf0c4b74e5e5147d0cd81c51ec

#### [v0.0.7](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.6...v0.0.7)

> 10 March 2023

- changed mtot to mtotal !104
- added an option to use event spin instead of spin1z=spin2z=0 !103
- simplifying plot_intrinsic_posterior !102
- Add feature to use search bias bounds study in init range !101
- fixed submit_at_exact_signal_position !97
- using pin-params option c66230abfc460001f8ec01b07731c8ea6c9eac56
- Making use_event_spin optional c65672ccf58bc6611f020e2fa13dc4df65187065
- updated to rapid-pe version 0.0.4 ca80e906b696fcae52d32d4686a175930e47b513

#### [v0.0.6](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.5...v0.0.6)

> 1 December 2022

- Increment RapidPE version to 0.0.3 !98
- added iteration level in integrate.sub and ILE log files. !95
- removed additional dir structure from output_dir !96
- added sigma as an option in postscripts !94
- added web_dir as an option  in config !92
- added itertaion level e84a70938f64a8012fcdbd01820cae121bd3bcb2

#### [v0.0.5](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.4...v0.0.5)

> 8 November 2022

- upgraded to rapid_pe==0.0.2 and rift==0.0.15.7 !90
- added more time histograms !88
- Switching 'publish' job's image from alpine to python !89
- added function name in cprofile !87
- cprofile summary page !83
- download coinc.xml !84
- Generalizing our SVD bin selection method !80
- changing rapipe and rift version to most recent release in pypi !82
- Adding shebang to post scripts !81
- gracedb page linked in summary page !75
- some functions to copy stylesheet.css 5d47406c318f0bfb676662f14c84c7a6f0aa6b6c
- fixing urlpath f0305f7ded49474d009f713e823b50eeb65ac121

#### [v0.0.4](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.3...v0.0.4)

> 16 September 2022

- Replacing use of unsupported shell feature !78
- saving timing info !76

#### [v0.0.3](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.2...v0.0.3)

> 13 September 2022

- Attempting to fix RTD builds for tagged releases !74

#### [v0.0.2](https://git.ligo.org/rapidpe-rift/rapidpe-rift-pipe/compare/v0.0.1...v0.0.2)

> 13 September 2022

- Triggering ReadTheDocs using CI/CD !72
- Initializing Sphinx documentation !61
- Removing outdated warning about BNS waveform approximant !64
- added `tabulate` in setup.cfg !63
- approximant as config option !62
- Specify prior range from svd bins !60
- Fixing author list !42
- Fix summary page !59
- list failed jobs in summary page !57
- lalapps_path2cache to lal_paths2cache !58
- fixed copying issue in  config.integrate_likelihood_cmds_dict !55
- updates to use manual-logarithm-offset !52
- bug fix in multinomial: sum of probabilities in multinomial is set to 1 !54
- Partial conversion of some subprocesses to modules !50
- Improvements in posterior script !51
- fixed posterior plots !49
- Fixing config issues !48
- Making initial region and overlap bank mutually exclusive !46
- Config parser unit tests !47
- Using config.py consistently in all scripts !45
- Adding ability to specify frame data types by IFO !36
- Iterate through the overlap files only if the initial region is not specified in the config file !38
- Added separate `overlap-threshold` for InitialGridOnly and GridRefine sections !44
- src/rapidpe_rift_pipe/cli.py: download coinc.xml as psd if psd.xml.gz does not exist !39
- Removed unused scripts !41
- Providing default executables !37
- improvements in post scripts to work with ligo.lw output files !34
- Adding ability to configure GraceDB backend URL !35
- removed unused scripts d5a2ed9dc8d74216e4db83ec30a838c2d3d76bd4
- improvements in post scripts eec28f90c8ec2d1a9c009d6f4d87c99bc7ab9b83
- updated post scripts to use  rift results file dc759809446862a77548ca2b851f046e1e35973e

#### v0.0.1

> 14 July 2022

- Specifying version numbers for rapidPE and RIFT !31
- added additional options in config to use rift ILE !29
- fixed loop over injections/event !28
- fixed paths and module imports !27
- Converted from unstructured files to a pip-installable package !26
- Fixed rapide-rift_pipe.py to itereate over injections !25
- added post scripts !23
- fixed injection ini file !24
- updated injection config file !22
- Fixed working directory issue and a mistyped variable !21
- Adding intrinsic-param=... arguments to config files !19
- Adding config parser options for limiting initial parameter regions !18
- combined generate_initial_grid_based_of_gstlal_O2_overlaps.py and rapidpe-rift_pipe.py !17
- Un-hard-coding the overlap bank location !16
- Made EventConfig parser reusable for other sections !15
- Cleaning up rapidpe-rift_pipe.py !14
- replaced tabs with four spaces !13
- added extra options in config_demo_sname.ini to use riftILE !12
- testing new config file !11
- RTPE dag submission script !10
- adding RTPE submission script !9
- Event section config parser !8
- Allowed for no injections file in config !6
- Converted two more scripts to use config.py !5
- Reproducibility tests and config consolidation !4
- converted lvalert script to py3 !3
- Adding basic testing infrastructure !2
- Python3.8-version-for-injections !1
- Test config files for the -I/--initial-range option 797c235afe0a5d141cd90fd61dd9721e7aa541e0
- added post scripts to 5f1235228ab460c5cd06f38c44a099806fe3a91b
- some improvements in posterior script to use other coordinates 7a3eed63a759b31850fac1cd5fee2edd6424ad82
