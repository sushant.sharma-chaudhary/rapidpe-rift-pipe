# Sorts the "arguments" field in condor sub files by --option names.
# Used to remove randomness from files generated for testing purposes.
import re
import sys
import argparse
import shlex
from typing import List

from dataclasses import dataclass, field

def make_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("input_sub_file")

    return parser


def main():
    cli_parser = make_parser()
    cli_args = cli_parser.parse_args()

    output_sub_file = f"{cli_args.input_sub_file}.sorted"
    create_sorted_sub_file(cli_args.input_sub_file, output_sub_file)


def create_sorted_sub_file(input_fname: str, output_fname: str):
    with open(input_fname,  "r") as input_file, \
         open(output_fname, "w") as output_file:
        for line in input_file:
            if line.startswith("arguments"):
                output_file.write(sort_arguments(line))
            else:
                output_file.write(line)


def sort_arguments(line: str) -> str:
    # Will store all of our options in a sortable manner.
    options: List[Option] = []

    # Parse the unsorted text.
    unsorted_text = (
        re.match("^arguments\\s+=\\s+\"(.*)\"$", line.strip())
          .group(1)
          .replace('""', '"') # Replace Condor's special '""' with '"'
    )

    # Get all tokens in the unsorted text.
    tokens = shlex.split(unsorted_text)

    # Parse the options
    opt_name = None
    opt_args = []
    for token in tokens:
        # Token is an option name.
        if token.startswith("--"):
            if opt_name is not None:
                # Store the previous option and its arguments.
                options.append(Option(name=opt_name, args=opt_args))
                # Clear the previous option's arguments.
                opt_args = []
            
            # Store the token as the current option's name.
            opt_name = token

        # Token is an option argument.
        else:
            opt_args.append(token)

    # Store the last option.
    options.append(Option(name=opt_name, args=opt_args))


    # opt_name = None
    # opt_args = []
    # i = 0
    # while i < len(unsorted_text):
    #     if unsorted_text[i] == " ":
    #         i += 1
    #         continue

    #     if unsorted_text[i:i+2] == "--":
    #         j = unsorted_text.index(" ")
    #         opt_name = unsorted_text[i:j]
    #         opt_args, i = parse_args(unsorted_text, j)

    # Combine the options back into a string which has now been sorted.
    sorted_text = " ".join(str(opt) for opt in sorted(options))

    return f"arguments = \"{sorted_text}\"\n"

'''
def parse_args(text: str, starting_index: int) -> tuple[list[str], int]:
    """
    Parses out the argument list in `text' starting at `starting_index'.
    Returns the list of arguments and the index after the list ends.
    """
    args: list[str] = []

    i = starting_index
    while i < len(text):
        if text[i:i+2] == "--":
            return args, i

        if text[i] == "'":
            ...

        else:
            j = text.index(
            args.append(text[i:j])
'''

@dataclass(order=True)
class Option:
    name: str
    args: List[str] = field(default_factory=list)

    def __str__(self):
        return self.name + " " + " ".join(self.args)


if __name__ == "__main__":
    sys.exit(main())
