"""
Extension for documenting INI files.

Copied from <https://stackoverflow.com/a/58579032>
"""
from sphinx.application import Sphinx
from sphinx.util.docfields import Field


def setup(app: Sphinx):
    app.add_object_type(
        'confval',
        'confval',
        objname='configuration value',
        indextemplate='pair: %s; configuration value',
        doc_field_types=[
            Field('type', label='Type', has_arg=False, names=('type',)),
            Field('default', label='Default', has_arg=False, names=('default',)),
        ]
    )

    return {
        'version': '1.0',
        'parallel_write_safe': True,
        'parallel_write_safe': True,
    }
