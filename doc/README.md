# Sphinx documentation

To build the docs locally, first install the required dependencies
```bash
$ pip install -r requirements.txt
```

Then start the build with
```bash
$ make html
```

You should now have files in `_build/html`.  If you'd like to view them in a browser, it's best to use an HTTP server.  A very easy to use (but not secure for real hosting) sever comes bundled with Python
```bash
$ cd _build/html
$ python -m http.server
```

Once that's running, you should be able to view the docs at http://0.0.0.0:8000/
