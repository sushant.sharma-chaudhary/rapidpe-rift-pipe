Welcome to RapidPE/RIFT Pipeline
================================

.. toctree::
   :maxdepth: 1
   :caption: Guide:

   usage/installation
   usage/quick-start
   usage/configuration


.. toctree::
   :maxdepth: 1
   :caption: Examples:

   examples/basic
   examples/ile-backends
   examples/initial-grids


.. todo::
   Add auto-API docs
